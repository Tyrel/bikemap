from django.test import TestCase
from django.core import management
from tracking.models import Track
from django.core.management.base import CommandError

# Create your tests here.
class ImportGPXManagementTest(TestCase):
    fixtures = ['tracking_test_fixtures.json',]


    def setUp(self):
        self.example_ride_file = "testing/example-ride.gpx"

    def test_user_does_not_exist(self):
        with self.assertRaisesMessage(CommandError, "User THISUSERWONTEXIST does not exist"):
            management.call_command('import_gpx', 'THISUSERWONTEXIST', 'foo')

    def test_path_exists(self):
        with self.assertRaisesMessage(CommandError, "/noop/nodir/ is neither an existing file, nor a path."):
            management.call_command('import_gpx', 'tyrel', '/noop/nodir/')

    def test_import(self):
        track_count = Track.objects.all().count()
        management.call_command('import_gpx', 'tyrel', 'testing/example-ride.gpx')
        new_track_count = Track.objects.all().count()
        self.assertNotEqual(track_count, new_track_count)


