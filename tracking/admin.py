from django.contrib import admin
from tracking.models import Track, Point, Segment


class TrackAdmin(admin.ModelAdmin):
    pass

class SegmentAdmin(admin.ModelAdmin):
    pass

class PointAdmin(admin.ModelAdmin):
    pass

# Register your models here.
admin.site.register(Track, TrackAdmin)
admin.site.register(Segment, SegmentAdmin)
admin.site.register(Point, PointAdmin)